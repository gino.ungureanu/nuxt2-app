export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Nuxt Starter',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  env: {
    apiKey: process.env.apiKey
  },

  publicRuntimeConfig: {
    test: 'public runtime config variable'
    // apiKey: process.env.apiKey
  },

  privateRuntimeConfig: {
    // apiKey: process.env.apiKey
  }
}


/*
varianta 1: pastram env, dar facem publish pt fiecare tara
varianta 2: populam publicRuntimeConfig, refactorizam peste tot process.env. sau env., facem publish o singura data
varianta 3: populam privateRuntimeConfig, dar facem proxy in nuxt cu serverMiddleware, nu mai expunem api keys
*/